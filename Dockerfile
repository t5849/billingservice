FROM openjdk:11-jre-slim AS run
COPY target/billing-latest.jar /usr/local/lib/app.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]
