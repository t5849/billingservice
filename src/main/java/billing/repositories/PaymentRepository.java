package billing.repositories;

import billing.models.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, UUID> {

    List<Payment> findPaymentsByTripIdAndSenderIdAndRecipientId(Long tripId, Long senderId, Long recipientId);

    List<Payment> findPaymentsByTripId(Long tripId);
//    List<Payment> findPaymentsByTripIdAndSenderId
}
