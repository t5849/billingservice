package billing.repositories;

import billing.models.Goal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoalRepository extends CrudRepository<Goal, Long> {
    List<Goal> getGoalByOwnerId(Long ownerId);
}
