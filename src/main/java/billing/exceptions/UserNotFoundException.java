package billing.exceptions;

import billing.models.Goal;
import org.springframework.http.ResponseEntity;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("User not found");
    }
}
