package billing.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExcHandler {

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<Error> handleException(Exception e) {
        return new ResponseEntity<>(new Error(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handle(Exception e) {
        e.printStackTrace(); //todo log stakeTrace
        return new ResponseEntity<>(new Error("Something went wrong."), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
