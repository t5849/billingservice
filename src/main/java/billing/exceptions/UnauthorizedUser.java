package billing.exceptions;

public class UnauthorizedUser extends RuntimeException {
    public UnauthorizedUser() {
        super("User is unauthorized");
    }
}
