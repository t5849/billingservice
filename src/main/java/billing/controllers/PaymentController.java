package billing.controllers;

import billing.exceptions.UnauthorizedUser;
import billing.models.Goal;
import billing.models.ParticipantInfo;
import billing.models.Payment;
import billing.services.Impl.GoalService;
import billing.services.Impl.PaymentService;
import billing.services.Validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
//@RequestMapping("")
@CrossOrigin("http://localhost:4200")
public class PaymentController {
    private final PaymentService paymentService;
    private final Validator validator;
    private final GoalService goalService;

    public PaymentController(PaymentService paymentService, Validator validator, GoalService goalService) {
        this.paymentService = paymentService;
        this.validator = validator;
        this.goalService = goalService;
    }

    private final String URL = "https://yoomoney.ru/quickpay/confirm.xml?receiver=%s&quickpay-form=%s&targets=%s&paymentType=%s&sum=%s&successURL=%s";
    private final String successURL = "https://traveling-together.hopto.org/success?paymentId=";

    @PostMapping(value = "/pay")
    public ResponseEntity<String> viewPaymentForm(@RequestBody Payment payment, @RequestHeader("Authorization") String token) {
        Long userId = validator.isTokenValid(token);
        if (userId != null && validator.isUserExist(userId)) {
            payment = paymentService.addPayment(payment);
            //RedirectView paymentForm = new RedirectView();
            var url = String.format(URL,
                    payment.getReceiver(),
                    payment.getQuickPayForm(),
                    payment.getTargets(),
                    payment.getPaymentType(),
                    payment.getSum(),
                    successURL + payment.getPaymentId());
            //paymentForm.setUrl(url);
            //todo!!!!! НЕ ЗАБЫТЬ УДАЛИТЬ
//            goalService.markGoalsAsPaidForUser(payment.getTripId(), payment.getSenderId(), payment.getRecipientId());
            return new ResponseEntity<>(url, HttpStatus.OK);
        } else {
            throw new UnauthorizedUser();
        }
    }

    @GetMapping(value = "/pay2")
    public RedirectView testPay(/*@RequestBody Payment payment*/) {
        Payment payment = new Payment(1, 1L, 2L, 1L);
        RedirectView paymentForm = new RedirectView();
        payment = paymentService.addPayment(payment);
        var url = String.format(URL,
                "4100117620712227",
                "shop",
                "kek",
                "AC",
                "2",
                "http://localhost:8080/lol?paymentId=" + payment.getPaymentId());
        paymentForm.setUrl(url);
        return paymentForm;
    }

//    @GetMapping("pay/success")
//    public ResponseEntity<Payment> isSuccessPayment(@RequestParam UUID paymentId) {
//        Payment payment = paymentService.getPaymentById(paymentId);
//        System.out.println(paymentId);
////        Payment payment = new Payment();
//        Goal goal = goalService.getGoal(payment.getGoalId());
//        ParticipantInfo info = goalService.getParticipantInfoByGoalIdAndUserId(goal.getGoalId(), goal.getOwnerId());
//        goal.getInfo().remove(info);
//        info.setPaid(true);
//        goal.getInfo().add(info);
//        goalService.updateGoal(goal);
//        payment.setSuccess(true);
//        return new ResponseEntity<>(paymentService.updatePayment(payment), HttpStatus.OK);
//    }

    @PostMapping("/pay/success")
    public ResponseEntity<Payment> isSuccessPayment(@RequestParam UUID paymentId) {
        Payment payment = paymentService.getPaymentById(paymentId);
//        System.out.println(paymentId);
//        Payment payment = new Payment();
//        Goal goal = goalService.getGoal(payment.getGoalId());
//        ParticipantInfo info = goalService.getParticipantInfoByGoalIdAndUserId(goal.getGoalId(), goal.getOwnerId());
//        goal.getInfo().remove(info);
//        info.setPaid(true);
//        goal.getInfo().add(info);
//        goalService.updateGoal(goal);

        //todo ВАЖНО!!!!!!!
        goalService.markGoalsAsPaidForUser(payment.getTripId(), payment.getSenderId(), payment.getRecipientId());
        payment.setSuccess(true);
        return new ResponseEntity<>(paymentService.updatePayment(payment), HttpStatus.OK);
    }



//    @GetMapping("/info2")
//    public ResponseEntity<HashMap<Long, Double>> getPayments(@RequestParam Long tripId, @RequestParam Long userId) {
//        return new ResponseEntity<>(paymentService.getPayments(tripId, userId), HttpStatus.OK);
//    }

    @GetMapping("/outPayments")
    public ResponseEntity<List<Payment>> getOutPayments(@RequestParam Long tripId, @RequestParam Long userId) {
        return new ResponseEntity<>(paymentService.getOutPayments(tripId, userId), HttpStatus.OK);
    }

    @GetMapping("/inPayments")
    public ResponseEntity<List<Payment>> getInPayments(@RequestParam Long tripId, @RequestParam Long userId) {
        return new ResponseEntity<>(paymentService.getInPayments(tripId, userId), HttpStatus.OK);
    }
}