package billing.controllers;

import billing.exceptions.UnauthorizedUser;
import billing.exceptions.UserNotFoundException;
import billing.models.Goal;
import billing.services.Impl.GoalService;
import billing.services.Validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/goals")
public class GoalController {
    private final GoalService goalService;
    private final Validator validator;

    public GoalController(GoalService goalService, Validator validator) {
        this.goalService = goalService;
        this.validator = validator;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Goal> getGoal(@PathVariable("id") long id, @RequestHeader("Authorization") String token) {
        var userId = validator.isTokenValid(token);
        if (userId != null) {
            if (/*validator.isUserExist(userId)*/ true) {
                return new ResponseEntity<>(goalService.getGoal(id), HttpStatus.OK);
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new UnauthorizedUser();
        }
    }

    @PostMapping()
    public ResponseEntity<Goal> createGoal(@RequestBody Goal goal, @RequestHeader("Authorization") String token) {
        var userId = validator.isTokenValid(token);
        if (userId != null) {
            if (/*validator.isUserExist(userId)*/ true) {
                return new ResponseEntity<>(goalService.addGoal(goal), HttpStatus.CREATED);
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new UnauthorizedUser();
        }
    }

    @DeleteMapping()
    public ResponseEntity<String> deleteGoalById(@RequestParam Long id, @RequestHeader("Authorization") String token) {
        Long userId = validator.isTokenValid(token);
        if (userId != null) {
            if (validator.isUserExist(userId)) {
                goalService.deleteGoalById(id);
                return new ResponseEntity<>("Successfully deleted", HttpStatus.OK);
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new UnauthorizedUser();
        }
    }

    @PutMapping
    public ResponseEntity<Goal> updateGoal(@RequestBody Goal goal, @RequestHeader("Authorization") String token) {
        Long userId = validator.isTokenValid(token);
        if (validator.isTokenValid()) {
            if (validator.isUserExist(userId)) {
                return new ResponseEntity<>(goalService.updateGoal(goal), HttpStatus.OK);
            } else {
                throw new UserNotFoundException();
            }
        } else {
            throw new UnauthorizedUser();
        }
    }

    @GetMapping("/info")
    public ResponseEntity<HashMap<Long, Double>> getInfo(@RequestParam Long tripId, @RequestParam Long userId) {
        return new ResponseEntity<>(goalService.calculateOwns(tripId, userId), HttpStatus.OK);
    }
}