package billing.services.Impl;

import billing.models.Goal;
import billing.models.ParticipantInfo;
import billing.models.Payment;
import billing.repositories.GoalRepository;
import billing.repositories.PaymentRepository;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GoalService {
    private final GoalRepository goalRepository;
    private final PaymentRepository paymentRepository;

    public GoalService(GoalRepository repo, PaymentRepository paymentRepository) {
        this.goalRepository = repo;
        this.paymentRepository = paymentRepository;
    }

//    private static final String URI_GET_USERS = "http://tripspltr.zapto.org:8083/trips/participants?tripId={tripId}";
//    private static final String URI_GET_USERS = "http://localhost:8083/trips/participants?tripId={tripId}";
    private static final String URI_GET_USERS = "http://trpsrv-instance:8083/trips/participants?tripId={tripId}";
    private static final String URI_GET_GOALS_IDS = "http://trpsrv-instance:8083/trips/goalsIds/{tripId}";
//    private static final String URI_GET_GOALS_IDS = "http://localhost:8083/trips/goalsIds/{tripId}";
    private static final String URI_GET_OWNER_ID = "http://trpsrv-instance:8083/trips/owner/{tripId}";
//    private static final String URI_GET_OWNER_ID = "http://localhost:8083/trips/owner/{tripId}";
    public Goal addGoal(Goal goal) {
        return goalRepository.save(goal);
    }

    public Goal updateGoal(Goal goal) {
        return goalRepository.save(goal);
    }

    public void deleteGoal(Goal goal) {
        goalRepository.delete(goal);
    }

    public void deleteGoalById(Long id) {
        goalRepository.findById(id).ifPresent(goalRepository::delete);
    }

    public Goal getGoal(Long goalId) {
        return goalRepository.findById(goalId).orElse(null);
    }

    public ParticipantInfo getParticipantInfoByGoalIdAndUserId(Long goalId, Long userId) {
        return Objects.requireNonNull(goalRepository.findById(goalId).orElse(null)).getInfo().stream().filter(x -> x.getParticipantId().equals(userId)).findFirst().orElse(null);
    }

    public HashMap<Long, Double> calculateOwns(Long tripId, Long userId) {
        return calculate(tripId, userId);
    }

    private HashMap<Long, Double> calculate(Long tripId, Long userId) {
        HashMap<Long, Double> result = new HashMap<>();
        List<Long> participantsId = getParticipants(tripId);
        List<Long> goalsIds = getGoalsIds(tripId);
        List<Payment> payments;
        Long ownerId = getOwnerId(tripId);
        double ownToPay;
        double ownToReceive;

        if (!ownerId.equals(userId)) {
            ownToReceive = calculateOwnToReceive(userId, ownerId, goalsIds);
            ownToPay = calculateOwnToPay(userId, ownerId, goalsIds);
//            payments = paymentRepository.findPaymentsByTripIdAndSenderIdAndRecipientId(tripId, userId, ownerId);
            result.put(ownerId, ownToReceive - ownToPay);
        }

        for (Long participantId : participantsId) {
            if (!participantId.equals(userId)) {
                ownToReceive = calculateOwnToReceive(userId, participantId, goalsIds);
                ownToPay = calculateOwnToPay(userId, participantId, goalsIds);
                result.put(participantId, ownToReceive - ownToPay);
            }
        }
        return result;
    }

    private double calculateOwnToReceive(Long userId, Long participantId, List<Long> goalsIds) {
        return goalsIds.stream()
                .map(goalRepository::findById)
                .filter(goal -> goal.isPresent())
                .map(goal -> goal.get())
                .filter(goal -> goal.getOwnerId().equals(userId))
                .filter(goal -> !goal.isPaid()) // неоплаченные полностью !!!!!
                .map(Goal::getInfo)
                .flatMap(Collection::stream)
                .filter(currentInfo -> currentInfo.getParticipantId().equals(participantId))
                .filter(currentInfo -> !currentInfo.isPaid()) // неоплаченные этим челом для которого вызываем метод (для userId)!!!!!
                .mapToDouble(ParticipantInfo::getSum)
                .sum();
    }

    private double calculateOwnToPay(Long userId, Long participantId, List<Long> goalsIds) {
        return goalsIds.stream()
                .map(goalRepository::findById)
                .filter(goal -> goal.isPresent())
                .map(Optional::get)
                .filter(goal -> goal.getOwnerId().equals(participantId))
                .filter(goal -> !goal.isPaid()) // неоплаченные полностью !!!!!
                .map(Goal::getInfo)
                .flatMap(Collection::stream)
                .filter(currentInfo -> currentInfo.getParticipantId().equals(userId))
                .filter(currentInfo -> !currentInfo.isPaid()) // неоплаченные этим челом для которого вызываем метод (для userId)!!!!!
                .mapToDouble(ParticipantInfo::getSum)
                .sum();
    }


    public void markGoalsAsPaidForUser(Long tripId, Long senderId, Long recipientId) {

        // здесь помечаем что сендер заплатил за все текущие голы где получатель являлся их хозяином
          List<Goal> goals = getGoalsIds(tripId).stream()
                   .map(goalRepository::findById)
                  .filter(goal -> goal.isPresent())
                   .map(Optional::get)
                  .filter(goal -> goal.getOwnerId().equals(recipientId))
                  .collect(Collectors.toList());
          goals.stream()
                   .map(Goal::getInfo)
                   .flatMap(Collection::stream)
                   .filter(currentInfo -> currentInfo.getParticipantId().equals(senderId))
                   .forEach(currentInfo -> currentInfo.setPaid(true));

        List<Goal> goals2 = getGoalsIds(tripId).stream()
                .map(goalRepository::findById)
                .filter(goal -> goal.isPresent())
                .map(Optional::get)
                .filter(goal -> goal.getOwnerId().equals(senderId))
                .collect(Collectors.toList());
        goals2.stream()
                .map(Goal::getInfo)
                .flatMap(Collection::stream)
                .filter(currentInfo -> currentInfo.getParticipantId().equals(recipientId))
                .forEach(currentInfo -> currentInfo.setPaid(true));


          /*
           тут помечаю голы (целые) как оплаченные полностью если все челы из списка учатсников заплатили
           получателю
           */
          for (Goal goal : goals) {
              List<ParticipantInfo> infos = goal.getInfo();
              int count = 0;
              for (ParticipantInfo currInfo : infos) {
                  if (currInfo.isPaid()) {
                      count += 1;
                  }
              }
              if (count == infos.size()) {
                  goal.setPaid(true);
              }
          }

        for (Goal goal : goals2) {
            List<ParticipantInfo> infos = goal.getInfo();
            int count = 0;
            for (ParticipantInfo currInfo : infos) {
                if (currInfo.isPaid()) {
                    count += 1;
                }
            }
            if (count == infos.size()) {
                goal.setPaid(true);
            }
        }

          // обновляем результат
          goalRepository.saveAll(goals);
          goalRepository.saveAll(goals2);
    }

    public List<Long> getParticipants(Long tripId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Long[]> response = restTemplate.exchange(
                URI_GET_USERS,
                HttpMethod.GET,
                entity,
                Long[].class,
                tripId);
        return Arrays.stream(response.getBody()).collect(Collectors.toList());
    }

    private List<Long> getGoalsIds(Long tripId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Long[]> response = restTemplate.exchange(
                URI_GET_GOALS_IDS,
                HttpMethod.GET,
                entity,
                Long[].class,
                tripId);
        return Arrays.stream(response.getBody()).collect(Collectors.toList());
    }

    public Long getOwnerId(Long tripId) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Long> response = restTemplate.getForEntity(
                URI_GET_OWNER_ID,
                Long.class,
                tripId);
        return response.getBody();
    }
}