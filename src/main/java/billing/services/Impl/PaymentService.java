package billing.services.Impl;

import billing.models.Goal;
import billing.models.Payment;
import billing.repositories.PaymentRepository;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PaymentService {
    private PaymentRepository repo;
    private GoalService goalService;

    public PaymentService(PaymentRepository repo, GoalService goalService) {
        this.repo = repo;
        this.goalService = goalService;
    }

    public Payment addPayment(Payment payment) {
        return repo.save(payment);
    }

    public Payment getPaymentById(UUID id) {
        return repo.findById(id).orElse(null);
    }

//    public HashMap<Long, Double> getPayments(Long tripId, Long userId) {
//        HashMap<Long, Double> result = new HashMap<>();
//        List<Payment> payments = repo.findPaymentsByTripId(tripId);
//        System.out.println(payments.size());
//        List<Long> participantsId = goalService.getParticipants(tripId);
//        Long ownerId = goalService.getOwnerId(tripId);
//        double resultSum;
//
//        if (!ownerId.equals(userId)) {
//            resultSum = - payments.stream()
//                    .filter(payment -> payment.getRecipientId().equals(ownerId))
//                    .mapToDouble(Payment::getSum)
//                    .sum();
//            result.put(ownerId, resultSum);
//        }
//
//        for (Long participantId : participantsId) {
//            if (!participantId.equals(userId)) {
//                resultSum = payments.stream()
//                        .filter(payment -> payment.getSenderId().equals(participantId))
//                        .mapToDouble(Payment::getSum)
//                        .sum();
//                result.put(participantId, resultSum);
//            }
//        }
//        return result;
//    }

    public List<Payment> getOutPayments(Long tripId, Long userId) {
        return repo.findPaymentsByTripId(tripId).stream()
                .filter(payment -> payment.getSenderId().equals(userId))
                .collect(Collectors.toList());
    }

    public List<Payment> getInPayments(Long tripId, Long userId) {
        return repo.findPaymentsByTripId(tripId).stream()
                .filter(payment -> payment.getRecipientId().equals(userId))
                .collect(Collectors.toList());
    }

    public Payment updatePayment(Payment payment) {
        return repo.save(payment);
    }
}
