package billing.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Payment {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID paymentId;

    @JsonInclude()
    @Transient
    private String receiver;

    @JsonInclude()
    @Transient
    private String quickPayForm;

    @JsonInclude()
    @Transient
    private String targets;

    @JsonInclude()
    @Transient
    private String paymentType;

    @JsonInclude()
    @Transient
    private String successURL;

    private double sum;

    private Long senderId;

    private Long recipientId;

//    private Long goalId;

    private Long tripId;

    private boolean success = false;

    public Payment(double sum, Long senderId, Long recipientId, Long tripId) {
        this.sum = sum;
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.tripId = tripId;
    }
}
