package billing.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Goal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long goalId;

    private Long ownerId;

    private boolean isPaid;

//    private Long senderId;

    @Column(name = "amount", columnDefinition = "integer default 0")
    private double amount;

//    private Long tripId;

    @ElementCollection
    @CollectionTable(
            name = "participantInfo",
            joinColumns = @JoinColumn(name = "goal_id")
    )
    private List<ParticipantInfo> info;
}
